export interface StorageOptions {
  namespace: string;
  name: string;
  storage: "session" | "local" | "memory";
}

export interface Menu {
  name: string;
  icon: string | { active: string; inactive: string };
  title: string;
  to: string;
  info: number;
}

export interface App {
  name: string;
  title: string;
  description: string;
  copyright: string;
}

export interface Http {
  prefix: string;
  timeout: number;
}

export interface Config {
  storageOptions: StorageOptions;
  menus: Array<Menu>;
  app: App;
  http: Http;
}

export default {
  storageOptions: {
    namespace: "vant__", // key prefix
    name: "ls", // name variable Vue.[ls] or this.[$ls],
    storage: "local" // storage name session, local, memory}
  },
  menus: [
    { name: "home", icon: "home-o", title: "首页", to: "home", info: 0 },
    { name: "search", icon: "search", title: "搜索", to: "search", info: 0 },
    {
      name: "friends",
      icon: "friends-o",
      title: "好友",
      to: "friends",
      info: 5
    },
    {
      name: "setting",
      icon: "setting-o",
      title: "设置",
      to: "setting",
      info: 20
    }
  ],
  app: {
    name: "Vant Wechat",
    title: "Vant Wechat",
    description: "基于Vant 开发的微信公众号",
    copyright: "Copyright &copy;  vant.kerbores.com"
  },
  http: {
    prefix: process.env.NODE_ENV === "production" ? "/" : "/api",
    timeout: 60 * 1000
  }
} as Config;
