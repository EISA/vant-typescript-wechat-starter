import Vue from "vue";
import { ValidationProvider, extend } from "vee-validate";
import * as rules from "vee-validate/dist/rules";
import { localize } from "vee-validate";
import zhCN from "vee-validate/dist/locale/zh_CN.json";

localize("zh_CN", zhCN);

for (const [rule, validation] of Object.entries(rules)) {
  extend(rule, {
    ...validation
  });
}

extend("secret", {
  validate: value => value === "example",
  message: "This is not the magic word"
});

// Register it globally
Vue.component("ValidationProvider", ValidationProvider);
