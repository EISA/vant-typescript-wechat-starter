import Vue from "vue";
import { default as WechatPlugin } from "./wechat";
import Storage from "vue-ls";
import config from "@/config";
import "./vee-validate";
import { axiosPlugin } from "./axios";
import { ApiPlugin } from "@/api/index";

Vue.use(WechatPlugin);
Vue.use(Storage, config.storageOptions);
Vue.use(axiosPlugin);
Vue.use(ApiPlugin);
