import {
  VuexModule,
  Module,
  Mutation,
  Action,
  getModule
} from "vuex-module-decorators";
import store from "@/store";

export interface UserState {
  name: string;
  openid: string;
  token: string;
}

@Module({ dynamic: true, store, name: "user" })
class User extends VuexModule implements UserState {
  name = "";
  openid = "";
  token = "";

  @Action
  public toUser() {
    return { name: this.name, openid: this.openid, token: this.token };
  }

  @Mutation
  public login(user: UserState) {
    Object.assign(this, user);
  }
}

export const UserModule = getModule(User);
