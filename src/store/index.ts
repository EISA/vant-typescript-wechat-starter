import Vue from "vue";
import Vuex from "vuex";
import { MenuState } from "./modules/menu";
import { UserState } from "./modules/user";

Vue.use(Vuex);

export interface RootState {
  menu: MenuState;
  user: UserState;
}
export default new Vuex.Store<RootState>({
  getters: {
    menus: (state: RootState) => state.menu.menus,
    token: (state: RootState) => state.user.token
  }
});
