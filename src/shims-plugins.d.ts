import Vue, { VueConstructor } from "vue";
import { WechatJsSdk } from "./plugins/wechat";
import { AxiosInstance } from "axios";
import { Api } from "./api/index";
declare global {
  interface Window {
    wx: WechatJsSdk;
    axios: AxiosInstance;
    api: Api;
  }
}

declare module "vue/types/vue" {
  interface Vue {
    wx: WechatJsSdk;
    $wechat: WechatJsSdk;
    axios: AxiosInstance;
    api: Api;
  }
  interface VueConstructor {
    $wechat: WechatJsSdk;
    axios: AxiosInstance;
    api: Api;
  }
}
