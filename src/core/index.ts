import Vue from "vue";
import { MenuModule } from "@/store/modules/menu";
import config from "@/config";
import { UserModule } from "@/store/modules/user";

export function Initializer(showMessage: boolean): void {
  console.log(showMessage);
  MenuModule.init(Vue.ls.get("menus", config.menus));
  UserModule.login(Vue.ls.get("user", { name: "", openid: "", token: "" }));
}

export function Persistor(): void {
  Vue.ls.set("menus", MenuModule.menus);
  Vue.ls.set("user", UserModule.toUser());
}
