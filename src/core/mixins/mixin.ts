import { Vue, Component } from "vue-property-decorator";
import { MenuModule } from "@/store/modules/menu";
import config from "@/config";
@Component
class Mixin extends Vue {
  public MenuModule = MenuModule;
  public app = config.app;
}
export { Mixin };
